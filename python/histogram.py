import uhal
import numpy as np
import sys
from collections import Counter


DEVICE = "x0"
CONNECTIONS_FILE = "file:///home/cmx/dmonk/gbt-cmds/serenity_connections.xml"


class Histogram:
    def __init__(self, manager):
        self.manager = manager


class Histogram2D(Histogram):
    def __init__(self, address_list, width):
        super.__init__(self)
        self.address_list = address_list
        self.width = width

    def read(self):
        data = []
        for address in self.address_list:
            data.append(readMemory(self.manager, address, self.width))
        return np.array(data)


def main():
    uhal.disableLogging()
    manager = uhal.ConnectionManager(CONNECTIONS_FILE)
    hw = manager.getDevice(DEVICE)
    width = 11
    # Read back values
    mem1 = readMemory(hw, "payload.mem1", width)
    mem2 = readMemory(hw, "payload.mem1", width)
    f = open("hist2d.csv", "w")
    for i in range(len(mem1)):
        f.write("%d,%d\n" % (mem1[i], mem2[i]))
    f.close()
    print("Stub total CIC0: ", sum(mem1))
    print("Stub total CIC1: ", sum(mem2))


if __name__ == '__main__':
    main()
