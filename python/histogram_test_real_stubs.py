import sys
import uhal
from ipbtools import writeAddress, readMemory
from modulefuncs import programFPGA, programModule, resetDTCAligner, waitForHistogram
import pandas as pd

DEVICE = "x0"


def main():
    package = sys.argv[1]
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(DEVICE)

    # Program FPGA
    programFPGA(package)
    # Program module
    programModule(
        verbose=True,
        reconfigure=True,
        disableBadChips=True,
        setGlobalThreshold="550"
    )
    # Reset DTC
    resetDTCAligner(hw)

    # Set Histogram window size
    writeAddress(hw, "payload.csr.windowH", 1)
    # Wait 30 seconds to get histogram values
    waitForHistogram(hw, 30)
    df = pd.DataFrame()
    df["CIC_0"] = readMemory(hw, "payload.mem1", 11)
    df["CIC_1"] = readMemory(hw, "payload.mem2", 11)
    df.to_csv("histogram_2D_300V_550_run_22.csv", index=True)


if __name__ == '__main__':
    uhal.disableLogging()
    main()
