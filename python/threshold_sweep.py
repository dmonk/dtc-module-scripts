import sys
import uhal
from ipbtools import writeAddress, readMemory
from modulefuncs import programFPGA, programModule, resetDTCAligner, waitForHistogram
import numpy as np
import pandas as pd

DEVICE = "x0"


def main():
    package = sys.argv[1]
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(DEVICE)

    # Program FPGA
    programFPGA(package)
    # Program module
    programModule(
        reconfigure=True,
        disableBadChips=True,
        setGlobalThreshold="550",
        verbose=True
    )
    # Reset DTC
    resetDTCAligner(hw)

    # Set Histogram window size
    writeAddress(hw, "payload.csr.windowH", 1)
    # Wait 30 seconds to get histogram values
    waitForHistogram(hw, 20)

    # Start sweep
    df0 = pd.DataFrame()
    df1 = pd.DataFrame()
    for threshold in np.arange(550, 600, 5, dtype=int):
        print(threshold)
        programModule(
            verbose=True,
            reconfigure=True,
            disableBadChips=True,
            setGlobalThreshold=str(threshold)
        )
        resetDTCAligner(hw)
        waitForHistogram(hw, 30)
        df0[str(threshold)] = readMemory(hw, "payload.mem1", 11)
        df1[str(threshold)] = readMemory(hw, "payload.mem2", 11)
    df0.to_csv("threshold_cic0_run_3.csv", index=True)
    df1.to_csv("threshold_cic1_run_3.csv", index=True)



if __name__ == '__main__':
    uhal.disableLogging()
    main()
