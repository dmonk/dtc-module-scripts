PACKAGE=$1
DEVICE=x0

# Program and run firmware
~/dtc/dtc_init.sh -0 $PACKAGE 
cd ~/kirika/k_serenity_test/Ph2_ACF_2
source setup.sh
ssh hwtest@b14-workbench-router "echo OP2 0 | nc 192.168.2.101 9221 && sleep 1 && echo OP2 1 | nc 192.168.2.101 9221 && sleep 1"
./bin/serenity_2s --reconfigure --disableBadChips --setGlobalThreshold 580

# Power cycle commands to be run from b14-workbench-router
# echo OP2 0 | nc 192.168.2.101 9221
# echo OP2 1 | nc 192.168.2.101 9221

# ipb
export K_IPB_TOP=/home/cmx/kirika/k_ipb
export PATH=${LPGBT_TEST_TOP}/scripts:${PATH}
export LD_LIBRARY_PATH=${K_IPB_TOP}/lib:${LD_LIBRARY_PATH}
export PATH=${K_IPB_TOP}/bin:${PATH}

# reset dtc
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "write node=payload.fe_ctrl.chan_sel value=0"
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "write node=payload.fe_chan.aligner_reset0 value=1"
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "write node=payload.fe_chan.aligner_reset1 value=1"
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "read node=payload.*"
echo
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "write node=payload.fe_chan.aligner_reset0 value=0"
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "write node=payload.fe_chan.aligner_reset1 value=0"
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "read node=payload.*"
ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "write node=payload.csr.windowH value=1"
echo 
for i in $(seq 1 40); do
	ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "read node=payload.*";
	echo;
	sleep .5;
done;

ipb --conFile=$PACKAGE/connections.xml --boardId $DEVICE "read node=payload.*"

python ~/dmonk/gbt-cmds/read_2d.py
