import sys
import uhal
from ipbtools import writeAddress
from modulefuncs import programFPGA, programModule, resetDTCAligner, waitForHistogram

DEVICE = "x0"


def main():
    package = sys.argv[1]
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(DEVICE)

    # Program FPGA
    programFPGA(package)
    # Program module
    programModule(
        verbose=True,
        reconfigure=True,
        sendStubOn=sys.argv[2],
        setAddress=sys.argv[3]
    )
    # Reset DTC
    resetDTCAligner(hw)

    # Set Histogram window size
    writeAddress(hw, "payload.csr.windowH", 1)
    # Wait 30 seconds to get histogram values
    waitForHistogram(hw, 20)


if __name__ == '__main__':
    uhal.disableLogging()
    main()
