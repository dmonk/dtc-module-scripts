def readAddress(device, address):
    data = device.getNode(address).read()
    device.dispatch()
    return data


def writeAddress(device, address, value):
    device.getNode(address).write(value)
    device.dispatch()


def readMemory(manager, address, width):
    data = manager.getNode(address).readBlock(int(2**width))
    manager.dispatch()
    return data
