from __future__ import print_function
import subprocess
import sys
import os
from contextlib import contextmanager
from ipbtools import readAddress, writeAddress
from time import sleep
from copy import deepcopy

DEVICE = "x0"
STATUS_MAP = {0: "ERROR", 1: "UNLOCKED", 2: "LOCKING", 4: "LOCKED"}


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def programFPGA(package):
    subprocess.call("~/dtc/dtc_init.sh -0 {}".format(package), shell=True)


def powerCycleModule():
    subprocess.call(
        'ssh hwtest@b14-workbench-router "echo OP2 0 | nc 192.168.2.101 9221 && sleep 1 && echo OP2 1 | nc 192.168.2.101 9221 && sleep 1"',
        shell=True
    )


def programModule(**kwargs):
    base_command = "source ./setup.sh && ./bin/serenity_2s "
    if "reconfigure" in kwargs:
        if kwargs["reconfigure"] is True:
            powerCycleModule()
            base_command += "--reconfigure "
    if "sendStubOn" in kwargs:
        base_command += "--sendStubOn {} ".format(kwargs["sendStubOn"])
    if "setAddress" in kwargs:
        base_command += "--setAddress {} ".format(kwargs["setAddress"])
    if "disableBadChips" in kwargs:
        if kwargs["disableBadChips"] is True:
            base_command += "--disableBadChips "
    if "setGlobalThreshold" in kwargs:
        base_command += "--setGlobalThreshold {} ".format(kwargs["setGlobalThreshold"])
    with cd("~/kirika/k_serenity_test/Ph2_ACF_2"):
        child = subprocess.Popen(base_command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        for line in iter(child.stdout.readline, ""):
            if "verbose" in kwargs:
                if kwargs["verbose"] is True:
                    print(line, end='')
            if "switch off the SEH" in line:
                child.stdin.write("\n")
            elif "switch on the SEH" in line:
                child.stdin.write("\n")


def resetDTCAligner(hwdevice):
    sleep_time = 0.1
    writeAddress(hwdevice, "payload.fe_ctrl.chan_sel", 0)
    sleep(sleep_time)
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset0", 1)
    sleep(sleep_time)
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset1", 1)
    sleep(sleep_time)
    status0 = readAddress(hwdevice, "payload.fe_chan.aligner_status0").value()
    sleep(sleep_time)
    status1 = readAddress(hwdevice, "payload.fe_chan.aligner_status1").value()
    sleep(sleep_time)
    print("Aligner status: 0: {} | 1: {}".format(
        STATUS_MAP[status0], STATUS_MAP[status1]
    ))
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset0", 0)
    sleep(sleep_time)
    writeAddress(hwdevice, "payload.fe_chan.aligner_reset1", 0)
    sleep(sleep_time)
    status0 = readAddress(hwdevice, "payload.fe_chan.aligner_status0").value()
    sleep(sleep_time)
    status1 = readAddress(hwdevice, "payload.fe_chan.aligner_status1").value()
    sleep(sleep_time)
    print("Aligner status: 0: {} | 1: {}".format(
        STATUS_MAP[status0], STATUS_MAP[status1]
    ))


def waitForHistogram(hwdevice, seconds, cycles=3):
    histogram_cycles = [0, 0]
    bin_value, bin_value_previous = [0, 0], [0, 0]
    for i in range(int(seconds*2)):
        bin_value_previous = deepcopy(bin_value)
        bin_value[0] = readAddress(hwdevice, "payload.csr.histogram0").value()
        bin_value[1] = readAddress(hwdevice, "payload.csr.histogram1").value()
        for i, value in enumerate(bin_value):
            if value < bin_value_previous[i]:
                histogram_cycles[i] += 1
        if min(histogram_cycles) >= cycles:
            break
        sys.stdout.write("\rHistogram max bins | 0: {:08x} | 1: {:08x}".format(*bin_value))
        sys.stdout.flush()
        sleep(0.5)
    print("\n")
